# Bookmarks
 - [Every Programmer Should Know :thinking:](https://github.com/mtdvio/every-programmer-should-know/)
 - [Periodic Table of DevOps Tools](https://xebialabs.com/periodic-table-of-devops-tools/)
 - [Linux Audit - Linux Security: Auditing, Hardening and Compliance](https://linux-audit.com/)
 - [Commandline Challenge](https://cmdchallenge.com/)
 - [API Security Checklist](https://github.com/shieldfy/API-Security-Checklist)
 - [On decentralized digital democracy.](https://github.com/DemocracyEarth/paper)
 
